package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.Assert.*;

public class TopDownTest {
    private AppController appController;
    private String topDownFileName = "src/test/java/data/intrebari_TopDown.txt";

    @Before
    public void setUp() throws Exception {
        appController = new AppController(topDownFileName);
    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(topDownFileName);
        writer.print("");
        writer.close();
    }

    @Test
    public void addNewIntrebareTest() {
        int initialCount = 0;
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "Matematica");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void createNewTestTest() {
        try {
            appController.createNewTest();
            assertTrue(false);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getStatisticaTest() {
        try {
            evaluator.model.Statistica statistica = appController.getStatistica();
            assertTrue(false);
        } catch (NotAbleToCreateStatisticsException e) {
            assertTrue(true);
        }
    }

    @Test
    public void PA_Test() {
        int initialCount = 0;
        assertEquals(appController.getIntrebari().size(), 0);

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "Matematica");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void PAB_Test() {
        int initialCount = 0;
        assertEquals(appController.getIntrebari().size(), 0);

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "Matematica");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);

            try {
                appController.createNewTest();
                assertTrue(false);
            } catch (NotAbleToCreateTestException e) {
                assertTrue(true);
            }

            } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void PABC_Test() {
        int initialCount = 0;
        assertEquals(appController.getIntrebari().size(), 0);

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "Matematica");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);

            try {
                appController.createNewTest();
                assertTrue(false);
            } catch (NotAbleToCreateTestException e) {
                assertTrue(true);

                try {
                    evaluator.model.Statistica statistica = appController.getStatistica();
                    assertTrue(statistica.getIntrebariDomenii().size() == 1);
                } catch (NotAbleToCreateStatisticsException ex) {
                    assertTrue(false);
                }
            }

        } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }
}