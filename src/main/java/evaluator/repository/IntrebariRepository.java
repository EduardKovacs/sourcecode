package evaluator.repository;

import java.io.*;
import java.util.*;


import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.exception.DuplicateIntrebareException;

public class IntrebariRepository {
	private String _filename;
	private List<Intrebare> intrebari;
	
	public IntrebariRepository(String _filename) {
		setIntrebari(new LinkedList<Intrebare>());
		this._filename = _filename;
		intrebari = new ArrayList<Intrebare>();
	}
	
	public void addIntrebare(Intrebare i) throws DuplicateIntrebareException, IOException {
		if(exists(i)) {
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(_filename, true));

		writer.write(i.toString() + "\n");
		writer.close();
		intrebari.add(i);
	}
	
	public boolean exists(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(){
		
		List<Intrebare> intrebari = new LinkedList<Intrebare>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(_filename));
			line = br.readLine();
			while(line != null){
				String[] elements = line.split(";");
				intrebare = new Intrebare(elements[0], elements[1], elements[2], elements[3], elements[4], elements[5]);
				intrebari.add(intrebare);

				line = br.readLine();
			}
		
		}
		catch (IOException | InputValidationFailedException e) {
			// TODO: handle exception
		}
		finally{
			try {
				br.close();
			} catch (IOException e) {
				// TODO: handle exception
			}
		}

		this.intrebari = intrebari;
		return intrebari;
	}
	
	public List<Intrebare> getIntrebari() {
		loadIntrebariFromFile();
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
